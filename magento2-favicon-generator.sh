#!/usr/bin/env bash
clear

FUNCTION_INDICATOR="******* "
PACKAGE_NAME="grunt-real-favicon"

M2_FRONTEND_PATH="app/design/frontend"
VENDOR_NAME="Inchoo"
THEME_NAME="favicons"

APP_NAME="Some app"
BG_COLOR="red"
TEXT_COLOR="blue"

GRUNT_TASK_NAME="favicon-generator"
GRUNT_TASK_FILE="$GRUNT_TASK_NAME.js"
GRUNT_TASK_PATH_PREFIX="./dev/tools/grunt/tasks"
GRUNT_TASK_PATH="$GRUNT_TASK_PATH_PREFIX/$GRUNT_TASK_FILE"

IMAGE_LINK="https://pbs.twimg.com/profile_images/556065247161643009/jDHch5Fh.png"
FAVICON_WEB_FOLDER_NAME="web"
FAVICON_OUTPUT_FOLDER_NAME="favicons"
FAVICON_SOURCE_FOLDER_NAME="source"
FAVICON_OUTPUT_PATH="$FAVICON_WEB_FOLDER_NAME/$FAVICON_OUTPUT_FOLDER_NAME"
FAVICON_SOURCE_PATH="$FAVICON_OUTPUT_PATH/$FAVICON_SOURCE_FOLDER_NAME"
FAVICON_FILE="favicon.png"

installPackage() {
    echo "$FUNCTION_INDICATOR step 1: checking for npm dependency"

    if [npm ls | grep -q "$PACKAGE_NAME"]; then
        echo "found dependency $PACKAGE_NAME"
    else
        echo "installing dependency $PACKAGE_NAME"
        npm install --save-dev "$PACKAGE_NAME"
    fi
}

promptUserData() {
    echo "$FUNCTION_INDICATOR step 2: enter information"
    # printf "\nPlease specify vendor name: "
    # read VENDOR_NAME
    echo "vendor: $VENDOR_NAME"

    # printf "\nPlease specify theme name: "
    #read THEME_NAME
    echo "theme: $THEME_NAME"

    CURRENT_THEME="$M2_FRONTEND_PATH/$VENDOR_NAME/$THEME_NAME"
    mkdir -p "$CURRENT_THEME"

    # printf "\nPlease specify app name: "
    #read APP_NAME
    echo "app: $APP_NAME"

    # printf "\nPlease specify background color as HEX value: "
    #read BG_COLOR
    echo "bg color: $BG_COLOR"

    # printf "\nPlease specify text color: "
    #read TEXT_COLOR
    echo "text color: $TEXT_COLOR"
}

createGruntTaskFilename() {
    echo "$FUNCTION_INDICATOR step 4: create grunt task file"
    touch "$GRUNT_TASK_PATH"

    FILE_CONTENT="/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

module.exports = function (grunt) {
    'use strict';

    grunt.loadNpmTasks('grunt-real-favicon');

    grunt.registerTask('$GRUNT_TASK_NAME', function(){
        grunt.initConfig({
            realFavicon: {
                favicons: {
                    src: '$CURRENT_THEME/$FAVICON_SOURCE_PATH/$FAVICON_FILE',
                    dest: '$CURRENT_THEME/$FAVICON_OUTPUT_PATH',
                    options: {
                        iconsPath: '$FAVICON_OUTPUT_FOLDER_NAME',
                        html: [ '$CURRENT_THEME/$FAVICON_OUTPUT_PATH/favicon.html' ],
                        design: {
                            ios: {
                                pictureAspect: 'noChange'
                            },
                            desktopBrowser: {},
                            windows: {
                                pictureAspect: 'noChange',
                                backgroundColor: '$BG_COLOR',
                                onConflict: 'override'
                            },
                            androidChrome: {
                                pictureAspect: 'noChange',
                                themeColor: '$TEXT_COLOR',
                                manifest: {
                                    name: '$APP_NAME',
                                    display: 'browser',
                                    orientation: 'notSet',
                                    onConflict: 'override',
                                    declared: true
                                }
                            },
                            safariPinnedTab: {
                                pictureAspect: 'silhouette',
                                themeColor: '$TEXT_COLOR'
                            }
                        },
                        settings: {
                            scalingAlgorithm: 'Mitchell',
                            errorOnImageTooSmall: false
                        }
                    }
                }
            }
        });
        /* generate the favicons */
        grunt.task.run('realFavicon')
    });
};"

    echo "$FILE_CONTENT" > "$GRUNT_TASK_PATH"
}

# this may be for versions prior to 2.1.0.
# '
# registerGruntTask() {
#    echo "$FUNCTION_INDICATOR step 5: update Gruntfile.js"
#    if ! grep -q "$GRUNT_TASK_NAME" Gruntfile.js ; then
#        echo "updating task list in Gruntfile.js"
#        # TODO replace task name
#        #sed -i -e 's/'\''time\-grunt'\''/taskDir\ \+\ '\''\/favicon\-generator'\''\,\
#        #'\''time\-grunt'\''/' ./Gruntfile.js
#        #rm -f ./Gruntfile.js-e
#    else
#        echo "Gruntfile.js has already been updated"
#    fi
#}

downloadPreviewImage () {
    echo "$FUNCTION_INDICATOR step 3: download favicon image"

    FAVICON_PATH="$CURRENT_THEME/$FAVICON_SOURCE_PATH"

    if [ ! -f "$FAVICON_PATH/$FAVICON_FILE" ]; then
        echo "downloading preview image"
        mkdir -p "$FAVICON_PATH"
        wget -c -q "$IMAGE_LINK" -O "$FAVICON_PATH/$FAVICON_FILE"
    else
        echo "favicon already exists"
    fi
}

runGruntTask() {
    grunt "$GRUNT_TASK_NAME"
}

updateHtmlFile() {
    echo "$FUNCTION_INDICATOR step 6: update link tags"

    OUTPUT_ROOT_RELATIVE_PATH="$CURRENT_THEME/$FAVICON_OUTPUT_PATH"

    mkdir -p "$CURRENT_THEME/Magento_Theme/$FAVICON_WEB_FOLDER_NAME"
    cp "$CURRENT_THEME/$FAVICON_OUTPUT_PATH/favicon.ico" "$CURRENT_THEME/Magento_Theme/$FAVICON_WEB_FOLDER_NAME/"

    # echo '<meta name="msapplication-navbutton-color" content="'$BG_COLOR'" />' >> "$OUTPUT_ROOT_RELATIVE_PATH/favicon.html"
    echo "adding apple mobile meta tags"
    echo '
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />' >> "$OUTPUT_ROOT_RELATIVE_PATH/favicon.html"


    echo "replacing 'href' -> 'src'"
    sed -i -e 's/href/src/g' "$OUTPUT_ROOT_RELATIVE_PATH/favicon.html"

    # override default magento icon
    echo "replacing 'icon' -> 'shortcut icon'"
    sed -i -e 's/\"icon\"/\"icon shortcut\"/g' "$OUTPUT_ROOT_RELATIVE_PATH/favicon.html"

    # make link tag valid in XML
    echo "replacing '\">' -> '\" />'"
    sed -i -e 's/\"\>/\"\ \/\>/g' "$OUTPUT_ROOT_RELATIVE_PATH/favicon.html"

    #sed -i -e "s/.png/.png?$EPOCH/g" "$OUTPUT_ROOT_RELATIVE_PATH/manifest.json"
    #rm -f "$OUTPUT_ROOT_RELATIVE_PATH/manifest.json-e"

    removeMsTagsAndFiles

    rm -f "$OUTPUT_ROOT_RELATIVE_PATH/favicon.html-e"
}

removeMsTagsAndFiles() {
    echo "$FUNCTION_INDICATOR step 7: MS cleanup in favicon.html"

    sed -i -e '/msapplication-TileColor/d' "$OUTPUT_ROOT_RELATIVE_PATH/favicon.html"
    sed -i -e '/msapplication-TileImage/d' "$OUTPUT_ROOT_RELATIVE_PATH/favicon.html"
    sed -i -e '/msapplication-config/d' "$OUTPUT_ROOT_RELATIVE_PATH/favicon.html"

    rm -f "$OUTPUT_ROOT_RELATIVE_PATH/mstile-"*
    rm -f "$OUTPUT_ROOT_RELATIVE_PATH/browserconfig.xml"
}

setupXmlFile() {
     echo "$FUNCTION_INDICATOR step 7: prepare XML file"

     XML_PATH="$CURRENT_THEME/Magento_Theme/layout/"
     mkdir -p "$XML_PATH"

     XML_FILE="$XML_PATH/default_head_blocks.xml"

     if [ ! -f "$XML_FILE" ]; then

        updateHtmlFile

        echo "creating xml file"
        touch "$XML_FILE"

        HTML_CONTENT=$(<"$CURRENT_THEME/$FAVICON_OUTPUT_PATH/favicon.html")

        XML_CONTENT='
<?xml version="1.0"?>
<!--
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
-->
<page xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
    <head>
        '$HTML_CONTENT'
    </head>
</page>'
    echo "$XML_CONTENT" > "$XML_FILE"
     else
        echo "xml file already exists. to finish the script, copy content of $CURRENT_THEME/$FAVICON_OUTPUT_PATH/favicon.html into $XML_FILE"
        echo "then paste in terminal "
        echo "grunt clean:$THEME_NAME"
        echo "grunt exec:$THEME_NAME"
        echo "php bin/magento cache:clean"
        echo "php bin/magento setup:static-content:deploy"
        exit 1
     fi
}

deployData() {
    echo "$FUNCTION_INDICATOR step 8: deploying data"
    grunt clean:"$THEME_NAME"
    grunt exec:"$THEME_NAME"
    php bin/magento cache:clean
    php bin/magento setup:static-content:deploy
}

installPackage
promptUserData
downloadPreviewImage
createGruntTaskFilename
# registerGruntTask
runGruntTask
setupXmlFile
deployData

# bug MS icon - https://github.com/magento/magento2/issues/5023